package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.Color
import pl.piszkod.chesster.piece.Piece
import pl.piszkod.chesster.piece._

case class Board(
  // format: off
  positionToPiece: Map[Position, Piece] =
    Map(Position(0, 0) -> Rook(Black),
      Position(1, 0) -> Knight(Black),
      Position(2, 0) -> Bishop(Black),
      Position(3, 0) -> Queen(Black),
      Position(4, 0) -> King(Black),
      Position(5, 0) -> Bishop(Black),
      Position(6, 0) -> Knight(Black),
      Position(7, 0) -> Rook(Black),
      Position(0, 1) -> FirstMovePawn(Black),
      Position(1, 1) -> FirstMovePawn(Black),
      Position(2, 1) -> FirstMovePawn(Black),
      Position(3, 1) -> FirstMovePawn(Black),
      Position(4, 1) -> FirstMovePawn(Black),
      Position(5, 1) -> FirstMovePawn(Black),
      Position(6, 1) -> FirstMovePawn(Black),
      Position(7, 1) -> FirstMovePawn(Black),

      Position(0, 6) -> FirstMovePawn(White),
      Position(1, 6) -> FirstMovePawn(White),
      Position(2, 6) -> FirstMovePawn(White),
      Position(3, 6) -> FirstMovePawn(White),
      Position(4, 6) -> FirstMovePawn(White),
      Position(5, 6) -> FirstMovePawn(White),
      Position(6, 6) -> FirstMovePawn(White),
      Position(7, 6) -> FirstMovePawn(White),
      Position(0, 7) -> Rook(White),
      Position(1, 7) -> Knight(White),
      Position(2, 7) -> Bishop(White),
      Position(3, 7) -> Queen(White),
      Position(4, 7) -> King(White),
      Position(5, 7) -> Bishop(White),
      Position(6, 7) -> Knight(White),
      Position(7, 7) -> Rook(White)
    ),
  //format: on
  turn: Color = White
) {

  def print: String = {
    val board = Range
      .inclusive(0, 8)
      .map(y =>
        Range
          .inclusive(0, 8)
          .map { x =>
            if (y == 8) s"[$x]"
            else if (x == 8) s"[$y]"
            else {
              val position = Position(x, y)
              positionToPiece.get(position) match {
                case Some(piece) => s"[${piece.unicodeSymbol}]"
                case None        => "[ ]"
              }
            }
          }
          .mkString(" ")
      )
      .mkString("\n")
    s"Turn: [$turn]\n$board"
  }

}

object Board {
  case class Position(x: Int, y: Int)
}
