package pl.piszkod.chesster

import pl.piszkod.chesster.movement.MoveExecutor
import pl.piszkod.chesster.movement.MoveExtractor
import zio.console.putStrLn
import zio.App
import zio.ExitCode
import zio.URIO
import zio.ZEnv

object Main extends App {

  override def run(args: List[String]): URIO[ZEnv, ExitCode] =
    logic(args.headOption.getOrElse("./moves/checkmate.txt")).provideSomeLayer(layers.all).exitCode

  def logic(filePath: String) =
    for {
      _     <- putStrLn(".. Time to chess!")
      moves <- MoveExtractor.getMoves(filePath)
      _     <- Game.play(moves)
    } yield ()

  private object layers {

    val env = ZEnv.live

    val moveExtractor = env >>> MoveExtractor.live

    val moveExecutor = env >>> MoveExecutor.live

    val game = moveExecutor ++ env >>> Game.live

    val all = env ++ moveExtractor ++ moveExecutor ++ game

  }

}
