package pl.piszkod.chesster.movement

import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.MoveExecutor.Failure.IllegalMoveFailure
import pl.piszkod.chesster.movement.MoveExecutor.Failure.IncorrectPlayerMoving
import pl.piszkod.chesster.movement.MoveExecutor.Failure.KingInCheck
import pl.piszkod.chesster.movement.MoveExecutor.Failure.LoggingFailure
import pl.piszkod.chesster.movement.MoveExecutor.Failure.NoKingFailure
import pl.piszkod.chesster.movement.MoveExecutor.Failure.NoPieceAtMoveOrigin
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.Color
import pl.piszkod.chesster.piece.FirstMovePawn
import pl.piszkod.chesster.piece.King
import pl.piszkod.chesster.piece.Piece
import zio.console.Console
import zio.macros.accessible
import zio.Has
import zio.IO
import zio.ZIO
import zio.ZLayer

@accessible
case object MoveExecutor {

  type MoveExecutor = Has[Service]

  trait Service {
    def executeMove(move: Move, board: Board): IO[Failure, Board]
  }

  val live = ZLayer.fromService((console: Console.Service) =>
    new Service {

      override def executeMove(move: Move, board: Board): IO[Failure, Board] =
        for {
          _        <- console.putStrLn(s".. Trying to execute move: [$move]").mapError(LoggingFailure(_))
          _        <- showCheckMessage(board)
          piece    <- getPieceToMove(move, board)
          _        <- ensureMoveCorrectness(piece, move, board)
          newBoard <- performMove(piece, move, board)
        } yield newBoard

      private def getPieceToMove(move: Move, board: Board): IO[Failure, Piece] =
        board.positionToPiece.get(move.from) match {
          case Some(piece) => IO.succeed(piece)
          case None        => IO.fail(NoPieceAtMoveOrigin(move))
        }

      private def showCheckMessage(board: Board) =
        checkForCheck(board).catchSome {
          case _: KingInCheck => console.putStrLn(s"!! Player [${board.turn}] is in check").mapError(LoggingFailure(_))
        }

      private def ensureMoveCorrectness(piece: Piece, move: Move, board: Board): IO[Failure, Unit] =
        for {
          _ <- ensureThatCorrectPlayerIsMoving(piece, board)
          _ <- ensureThatMoveIsLegal(piece, move, board)
          _ <- ensureThatCurrentPlayerIsNotInCheck(piece, board)
        } yield ()

      private def performMove(piece: Piece, move: Move, board: Board): IO[Failure, Board] =
        for {
          _ <- console.putStrLn(s".. Executing move: [$move]").mapError(LoggingFailure(_))
          pieceToBePlaced = piece match {
                              case FirstMovePawn(color) => ActivePawn(color)
                              case piece                => piece
                            }
          newBoard = board.copy(
                       positionToPiece = board.positionToPiece.removed(move.from).updated(move.to, pieceToBePlaced),
                       turn = board.turn.opposite
                     )
        } yield newBoard

      private def ensureThatMoveIsLegal(piece: Piece, move: Move, board: Board): IO[Failure, Unit] =
        ZIO.fail(IllegalMoveFailure(move)).unless(piece.isMoveLegal(move, board))

      private def ensureThatCurrentPlayerIsNotInCheck(pieceToBeMoved: Piece, board: Board): IO[Failure, Unit] =
        pieceToBeMoved match {
          case King(_) => ZIO.unit
          case _       => checkForCheck(board)
        }

      private def checkForCheck(board: Board) =
        board.positionToPiece.find(keyValue => keyValue._2 == King(board.turn)) match {
          case Some((position, king @ King(_))) => IO.fail(KingInCheck(king)).unless(king.isNotInCheck(position, board))
          case _                                => IO.fail(NoKingFailure(board.turn))
        }

      private def ensureThatCorrectPlayerIsMoving(piece: Piece, board: Board): IO[Failure, Unit] =
        ZIO.fail(IncorrectPlayerMoving(piece, board.turn)).unless(piece.color == board.turn)

    }
  )

  trait Failure

  object Failure {
    final case class LoggingFailure(throwable: Throwable) extends Failure
    final case class NoKingFailure(color: Color) extends Failure

    final case class NoPieceAtMoveOrigin(move: Move) extends Failure
    final case class IllegalMoveFailure(move: Move) extends Failure
    final case class IncorrectPlayerMoving(piece: Piece, color: Color) extends Failure
    final case class KingInCheck(king: King) extends Failure
  }

}
