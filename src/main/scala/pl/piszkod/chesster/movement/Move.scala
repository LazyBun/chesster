package pl.piszkod.chesster.movement

import pl.piszkod.chesster.Board.Position

final case class Move(from: Position, to: Position)
