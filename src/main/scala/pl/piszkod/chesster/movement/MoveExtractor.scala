package pl.piszkod.chesster.movement

import pl.piszkod.chesster.userinput.UserInputFile
import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.MoveExtractor.Failure.BadMoveFormatFailure
import pl.piszkod.chesster.movement.MoveExtractor.Failure.FileReadFailure
import pl.piszkod.chesster.movement.MoveExtractor.Failure.LoggingFailure
import zio.console.Console
import zio.macros.accessible
import zio.Has
import zio.IO
import zio.ZIO
import zio.ZLayer

import scala.annotation.tailrec
import scala.util.Try

@accessible
case object MoveExtractor {

  type MoveExtractor = Has[Service]

  trait Service {
    def getMoves(filePath: String): IO[Failure, List[Move]]
  }

  val live = ZLayer.fromService((console: Console.Service) =>
    new Service {

      override def getMoves(filePath: String): IO[Failure, List[Move]] =
        for {
          _           <- console.putStrLn(s".. Getting moves from file [$filePath]...").mapError(LoggingFailure(_))
          inputReader <- ZIO.fromTry(Try(new UserInputFile(filePath))).orElseFail(FileReadFailure(filePath))
          moves       <- getMovesFromFile(inputReader)
          _           <- console.putStrLn(s".. Got moves from file [$filePath]: [${moves}] ").mapError(LoggingFailure(_))
        } yield moves

      @tailrec
      private def getMovesFromFile(inputReader: UserInputFile, list: List[Move] = List.empty): IO[Failure, List[Move]] =
        Option(inputReader.nextMove()) match {
          case Some(Array(fromX, fromY, toX, toY)) =>
            getMovesFromFile(inputReader, list.appended(Move(Position(fromX, fromY), Position(toX, toY))))
          case None                                =>
            IO.succeed(list)
          case _                                   =>
            IO.fail(BadMoveFormatFailure)
        }

    }
  )

  trait Failure

  object Failure {
    final case class FileReadFailure(filePath: String) extends Failure
    final case object BadMoveFormatFailure extends Failure
    final case class LoggingFailure(throwable: Throwable) extends Failure
  }

}
