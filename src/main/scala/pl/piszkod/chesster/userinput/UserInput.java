package pl.piszkod.chesster.userinput;

import java.io.IOException;

/*
 * Provided by 3rd party.
 */
public interface UserInput {
    int[] nextMove() throws IOException;
}

