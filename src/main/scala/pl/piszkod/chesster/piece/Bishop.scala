package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Piece.boundsFilter
import pl.piszkod.chesster.piece.Piece.friendliesFilter
import pl.piszkod.chesster.piece.Piece.skippingOverFilter

final case class Bishop(color: Color) extends Piece {

  override val unicodeSymbol: String = color match {
    case Color.White => "\u265D"
    case Color.Black => "\u2657"
  }

  override def isMoveLegal(move: Move, board: Board): Boolean = {
    val validMoves = List((1, 1), (1, -1), (-1, 1), (-1, -1))
      .map { offsets =>
        val columns = Range.inclusive(move.from.y + offsets._1, 7 * offsets._1, offsets._1)
        val rows = Range.inclusive(move.from.x + offsets._2, 7 * offsets._2, offsets._2)
        rows.zip(columns).map(pair => Position(pair._1, pair._2)).toList
      }

    validMoves
      .flatMap(skippingOverFilter(_, board))
      .filter(boundsFilter)
      .filter(position => friendliesFilter(color, position, board))
      .contains(move.to)
  }

}
