package pl.piszkod.chesster.piece

sealed trait Color {
  def opposite: Color
}

object Color {

  case object White extends Color {
    override def opposite: Color = Black
  }

  case object Black extends Color {
    override def opposite: Color = White
  }

}
