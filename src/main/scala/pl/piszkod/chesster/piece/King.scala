package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Piece.boundsFilter
import pl.piszkod.chesster.piece.Piece.friendliesFilter

final case class King(color: Color) extends Piece {

  override val unicodeSymbol: String = color match {
    case Color.White => "\u265A"
    case Color.Black => "\u2654"
  }

  override def isMoveLegal(move: Move, board: Board): Boolean = {
    val validMoves = Set(
      Position(move.from.x - 1, move.from.y),
      Position(move.from.x + 1, move.from.y),
      Position(move.from.x + 1, move.from.y + 1),
      Position(move.from.x + 1, move.from.y - 1),
      Position(move.from.x - 1, move.from.y + 1),
      Position(move.from.x - 1, move.from.y - 1),
      Position(move.from.x, move.from.y - 1),
      Position(move.from.x, move.from.y + 1)
    )

    validMoves
      .filter(boundsFilter)
      .filter(position => friendliesFilter(color, position, board))
      .contains(move.to) && isNotInCheck(move.to, board)
  }

  // TODO This surely could be done cleaner
  def isNotInCheck(kingPosition: Position, board: Board): Boolean = {
    def isTheSameColor(color: Color) = color == this.color
    def isThisPiece(piece: Piece) = piece == this
    def pieceIsAboutToBeAttacked(position: Position) = position == kingPosition
    !board
      .positionToPiece
      .filterNot(positionToPiece =>
        isTheSameColor(positionToPiece._2.color) ||
          isThisPiece(positionToPiece._2) ||
          pieceIsAboutToBeAttacked(positionToPiece._1)
      )
      .exists { positionToPiece =>
        val (position, piece) = positionToPiece
        piece.isMoveLegal(movement.Move(position, kingPosition), board)
      }
  }

}
