package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move

trait Piece {
  val color: Color
  val unicodeSymbol: String
  def isMoveLegal(move: Move, board: Board): Boolean

}

object Piece {

  def boundsFilter(position: Position): Boolean =
    position.x >= 0 &&
      position.x <= 7 &&
      position.y >= 0 &&
      position.y <= 7

  // TODO: This can be done better
  def skippingOverFilter(possibleMoves: List[Position], board: Board, allowAttacking: Boolean = true): List[Position] = {
    var encounteredFirstFigure = false
    possibleMoves.flatMap { pos =>
      val currentPosition = Position(pos.x, pos.y)
      board.positionToPiece.get(currentPosition) match {
        case Some(_) =>
          if (encounteredFirstFigure)
            None
          else {
            encounteredFirstFigure = true
            if (allowAttacking) Some(currentPosition) else None
          }
        case None    =>
          if (encounteredFirstFigure)
            None
          else
            Some(currentPosition)
      }
    }
  }

  def friendliesFilter(pieceColor: Color, position: Position, board: Board): Boolean =
    board.positionToPiece.get(position) match {
      case Some(piece) => piece.color != pieceColor
      case None        => true
    }

}
