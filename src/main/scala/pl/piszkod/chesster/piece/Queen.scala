package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move

final case class Queen(color: Color) extends Piece {

  override val unicodeSymbol: String = color match {
    case Color.White => "\u265B"
    case Color.Black => "\u2655"
  }

  private val rook = Rook(color)
  private val bishop = Bishop(color)

  override def isMoveLegal(move: Move, board: Board): Boolean =
    rook.isMoveLegal(move, board) || bishop.isMoveLegal(move, board)
}
