package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Piece.boundsFilter
import pl.piszkod.chesster.piece.Piece.friendliesFilter
import pl.piszkod.chesster.piece.Piece.skippingOverFilter

final case class Rook(color: Color) extends Piece {

  override val unicodeSymbol: String = color match {
    case Color.White => "\u265C"
    case Color.Black => "\u2656"
  }

  override def isMoveLegal(move: Move, board: Board): Boolean = {
    val validMoves = List((1, 0), (0, 1), (-1, 0), (0, -1))
      .map { offsets =>
        val columns =
          if (offsets._1 != 0) Range.inclusive(move.from.y + offsets._1, 7 * offsets._1, offsets._1).toList else List.fill(7)(move.from.y)
        val rows =
          if (offsets._2 != 0) Range.inclusive(move.from.x + offsets._2, 7 * offsets._2, offsets._2).toList else List.fill(7)(move.from.x)
        rows.zip(columns).map(pair => Position(pair._1, pair._2))
      }

    validMoves
      .flatMap(skippingOverFilter(_, board))
      .filter(boundsFilter)
      .filter(position => friendliesFilter(color, position, board))
      .contains(move.to)
  }

}
