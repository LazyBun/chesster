package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Piece.boundsFilter
import pl.piszkod.chesster.piece.Piece.friendliesFilter

final case class Knight(color: Color) extends Piece {

  override val unicodeSymbol: String = color match {
    case Color.White => "\u265E"
    case Color.Black => "\u2658"
  }

  override def isMoveLegal(move: Move, board: Board): Boolean = {
    val validMoves = Set(
      Position(move.from.x + 2, move.from.y - 1),
      Position(move.from.x + 2, move.from.y + 1),
      Position(move.from.x - 2, move.from.y - 1),
      Position(move.from.x - 2, move.from.y + 1),
      Position(move.from.x + 1, move.from.y - 2),
      Position(move.from.x + 1, move.from.y + 2),
      Position(move.from.x - 1, move.from.y - 2),
      Position(move.from.x - 1, move.from.y + 2)
    )

    validMoves
      .filter(boundsFilter)
      .filter(position => friendliesFilter(color, position, board))
      .contains(move.to)
  }

}
