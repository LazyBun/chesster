package pl.piszkod.chesster.piece

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.Board
import pl.piszkod.chesster.movement.Move

sealed trait Pawn extends Piece {

  override val unicodeSymbol: String = color match {
    // Symbols are reversed, because in intellij terminal black pieces looked white and white looked black (Dark theme)
    case Color.White => "\u265F"
    case Color.Black => "\u2659"
  }

  val moveLength: Int

  override def isMoveLegal(move: Move, board: Board): Boolean = {
    val colorMultiplier = color match {
      case Color.White => -1
      case Color.Black => 1
    }

    val regularMoves = Range
      .inclusive(1, moveLength)
      .map(length => Position(move.from.x, move.from.y + (length * colorMultiplier)))
      .toList

    val attackMoves = List(
      Position(move.from.x - 1, move.from.y + (1 * colorMultiplier)),
      Position(move.from.x + 1, move.from.y + (1 * colorMultiplier))
    ).flatMap { position =>
      board.positionToPiece.get(position) match {
        case Some(_) => Set(position)
        case _       => Set.empty
      }
    }

    (Piece.skippingOverFilter(regularMoves, board, allowAttacking = false) ++ attackMoves)
      .filter(Piece.boundsFilter)
      .filter(position => Piece.friendliesFilter(color, position, board))
      .contains(move.to)
  }

}

final case class FirstMovePawn(color: Color) extends Pawn {
  override val moveLength: Int = 2
}

final case class ActivePawn(color: Color) extends Pawn {
  override val moveLength: Int = 1
}
