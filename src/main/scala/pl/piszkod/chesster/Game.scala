package pl.piszkod.chesster

import pl.piszkod.chesster.Game.Failure.LoggingFailure
import pl.piszkod.chesster.Game.Failure.MoveExecutorFailure
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.movement.MoveExecutor
import pl.piszkod.chesster.movement.MoveExecutor.Failure.IllegalMoveFailure
import pl.piszkod.chesster.movement.MoveExecutor.Failure.IncorrectPlayerMoving
import pl.piszkod.chesster.movement.MoveExecutor.Failure.KingInCheck
import pl.piszkod.chesster.movement.MoveExecutor.Failure.NoPieceAtMoveOrigin
import zio.console.Console
import zio.macros.accessible
import zio.Has
import zio.IO
import zio.ZIO
import zio.ZLayer

@accessible
case object Game {

  type Game = Has[Service]

  trait Service {
    def play(moves: List[Move]): IO[Failure, Unit]
  }

  val live = ZLayer.fromServices((console: Console.Service, moveExecutor: MoveExecutor.Service) =>
    new Service {

      override def play(moves: List[Move]): IO[Failure, Unit] =
        for {
          _ <- console.putStrLn(s".. Starting to play the moves: [$moves]").mapError(LoggingFailure(_))
          // TODO: This could return the boards for testing purposes
          _ <- executeMoves(moves)
        } yield ()

      private def executeMoves(moves: List[Move], board: Board = Board()): IO[Failure, Unit] =
        moves match {
          case move :: tail =>
            for {
              newBoard <- moveExecutor
                            .executeMove(move, board)
                            .tap { newBoard =>
                              printMove(board, newBoard)
                            }
                            .catchAll {
                              case NoPieceAtMoveOrigin(move)           =>
                                console
                                  .putStrLn(s"XX Unable to move because there is no piece at move origin: [$move]")
                                  .mapBoth(LoggingFailure(_), _ => board)
                              case IllegalMoveFailure(move)            =>
                                console
                                  .putStrLn(s"XX Unable to move because the move is illegal: [$move]")
                                  .mapBoth(LoggingFailure(_), _ => board)
                              case IncorrectPlayerMoving(piece, color) =>
                                console
                                  .putStrLn(
                                    s"XX Unable to move because incorrect player wants to move. Current turn: [$color]. Wanted to move: [$piece]. Move: [$move]"
                                  )
                                  .mapBoth(LoggingFailure(_), _ => board)
                              case KingInCheck(king)                   =>
                                console
                                  .putStrLn(
                                    s"XX Unable to move because king is in check: [$king]"
                                  )
                                  .mapBoth(LoggingFailure(_), _ => board)
                              case error: MoveExecutor.Failure         =>
                                IO.fail(MoveExecutorFailure(error))
                              case error: LoggingFailure               =>
                                IO.fail(error)
                            }
              _        <- executeMoves(tail, newBoard)
            } yield ()
          case Nil          =>
            ZIO.unit
        }

      private def printMove(boardBefore: Board, boardAfter: Board) =
        for {
          _ <- console.putStrLn("----------------------------").mapError(LoggingFailure(_))
          _ <- console.putStrLn(boardBefore.print).mapError(LoggingFailure(_))
          _ <- console.putStrLn("~~~~~~~~~~~~INTO~~~~~~~~~~~~").mapError(LoggingFailure(_))
          _ <- console.putStrLn(boardAfter.print).mapError(LoggingFailure(_))
          _ <- console.putStrLn("----------------------------").mapError(LoggingFailure(_))
        } yield ()

    }
  )

  trait Failure

  object Failure {
    final case class LoggingFailure(throwable: Throwable) extends Failure
    final case class MoveExecutorFailure(error: MoveExecutor.Failure) extends Failure
  }

}
