package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.Rook
import zio.test.Assertion._
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test.assert
import zio.test.environment.TestEnvironment

object RookTest extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Movement Rook")(
      test("It is possible to move up") {
        val initialPosition = Position(4, 4)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = initialPosition.x, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move down") {
        val initialPosition = Position(4, 4)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = initialPosition.x, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move left") {
        val initialPosition = Position(4, 4)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = initialPosition.x - 2, y = initialPosition.y))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move right") {
        val initialPosition = Position(4, 4)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y))

        //then
        assert(result)(isTrue)
      },
      test("It is not possible to move through a piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Rook(White)
        val blockingPiecePosition = Position(6, 4)
        val blockingPiece = Rook(White)
        val board = Board(
          Map(
            initialPosition -> piece,
            blockingPiecePosition -> blockingPiece
          )
        )
        val move = Move(initialPosition, initialPosition.copy(x = initialPosition.x + 3, y = initialPosition.y))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It is possible to move attack first encountered piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Rook(White)
        val blockingPiecePosition = Position(6, 4)
        val blockingPiece = Rook(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            blockingPiecePosition -> blockingPiece
          )
        )
        val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isTrue)
      },
      test("Very weird move check") {
        val initialPosition = Position(4, 4)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 2))

        //then
        assert(result)(isFalse)
      },
      test("It should be impossible to leave board") {
        val initialPosition = Position(7, 7)
        val result = tryRookMove(initialPosition, initialPosition.copy(x = 7, y = 8))

        //then
        assert(result)(isFalse)
      }
    )

  private def tryRookMove = TestUtils.tryMovement(Rook(White))(_, _)

}
