package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.FirstMovePawn
import zio.test.Assertion.isTrue
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test.environment.TestEnvironment
import zio.test._
import zio.test.assert
import zio.test.Assertion._

object PawnTest extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Pawn tests")(
      suite("Movement FirstMovePawn")(
        test("It is possible to move by one forward") {
          //given
          val initialPosition = Position(4, 4)
          val piece = FirstMovePawn(White)
          val board = Board(
            Map(
              initialPosition -> piece
            )
          )
          val move = Move(initialPosition, initialPosition.copy(y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isTrue)
        },
        test("It is possible to move by two forward") {
          //given
          val initialPosition = Position(4, 4)
          val piece = FirstMovePawn(White)
          val board = Board(
            Map(
              initialPosition -> piece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 2))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isTrue)
        },
        test("It is not possible possible to move by one forward if square is occupied") {
          //given
          val initialPosition = Position(4, 4)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(White)
          val collidingPiecePosition = Position(4, 3)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isFalse)
        },
        test("It is not possible possible to move by two forward if square is occupied") {
          //given
          val initialPosition = Position(4, 4)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(White)
          val collidingPiecePosition = Position(4, 2)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 2))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isFalse)
        },
        test("It is not possible possible to move by two forward if square in between is occupied") {
          //given
          val initialPosition = Position(4, 4)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(White)
          val collidingPiecePosition = Position(4, 3)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 2))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isFalse)
        },
        test("It is possible to move to diagonal right square if it is occupied by enemy") {
          //given
          val initialPosition = Position(1, 1)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(Black)
          val collidingPiecePosition = Position(2, 0)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x + 1, y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isTrue)
        },
        test("It is possible to move to diagonal left square if it is occupied") {
          //given
          val initialPosition = Position(1, 1)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(Black)
          val collidingPiecePosition = Position(0, 0)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x - 1, y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isTrue)
        },
        test("It is not possible to move to diagonal left square if it is occupied by friendly") {
          //given
          val initialPosition = Position(1, 1)
          val piece = FirstMovePawn(White)
          val collidingPiece = ActivePawn(White)
          val collidingPiecePosition = Position(0, 0)
          val board = Board(
            Map(
              initialPosition -> piece,
              collidingPiecePosition -> collidingPiece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x - 1, y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isFalse)
        }
      ),
      suite("Movement ActivePawn")(
        test("It is possible to move by one forward") {
          //given
          val initialPosition = Position(4, 4)
          val piece = ActivePawn(White)
          val board = Board(
            Map(
              initialPosition -> piece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 1))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isTrue)
        },
        test("It is not possible to move by two forward") {
          //given
          val initialPosition = Position(4, 4)
          val piece = ActivePawn(White)
          val board = Board(
            Map(
              initialPosition -> piece
            )
          )
          val move = movement.Move(initialPosition, initialPosition.copy(y = initialPosition.y - 2))

          //when
          val result = piece.isMoveLegal(move, board)

          //then
          assert(result)(isFalse)
        }
      )
    )

}
