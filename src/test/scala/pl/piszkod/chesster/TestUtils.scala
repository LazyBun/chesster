package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Piece

object TestUtils {

  def tryMovement(piece: Piece)(initialPosition: Position, destination: Position): Boolean = {
    val board = Board(
      Map(
        initialPosition -> piece
      )
    )
    val move = Move(initialPosition, destination)

    piece.isMoveLegal(move, board)
  }

}
