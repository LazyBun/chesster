package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.movement.MoveExecutor
import pl.piszkod.chesster.movement.MoveExecutor.Failure._
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.King
import pl.piszkod.chesster.piece.Queen
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import zio.test.environment.TestEnvironment
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test._
import zio.test.assert
import zio.test.Assertion._
import zio.ZEnv
import zio.console.Console

object MoveExecutorTests extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Movement Executor")(
      testM("Should return NoPieceAtMoveOrigin if there is no piece at starting position") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(4, 4) -> King(White)
          )
        )
        val move = Move(Position(3, 3), Position(4, 4))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)
          .either

        //then
        assertM(result)(isLeft(equalTo(NoPieceAtMoveOrigin(move))))
      },
      testM("Should return NoKingFailure if there is king on board") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(4, 4) -> Queen(White)
          )
        )
        val move = Move(Position(4, 4), Position(5, 5))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)
          .either

        //then
        assertM(result)(isLeft(equalTo(NoKingFailure(White))))
      },
      testM("Should return IncorrectPlayerMoving if incorrect player takes his turn") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(2, 2) -> King(White),
            Position(4, 4) -> King(Black)
          )
        )
        val move = Move(Position(4, 4), Position(5, 5))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)
          .either

        //then
        assertM(result)(isLeft(equalTo(IncorrectPlayerMoving(King(Black), White))))
      },
      testM("Should return IllegalMoveFailure if move is illegal") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(4, 4) -> King(White)
          )
        )
        val move = Move(Position(4, 4), Position(7, 7))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)
          .either

        //then
        assertM(result)(isLeft(equalTo(IllegalMoveFailure(move))))
      },
      testM("Should return KingInCheck if player is in check and moves piece other than king") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(4, 4) -> King(White),
            Position(5, 5) -> ActivePawn(White),
            Position(2, 4) -> Queen(Black)
          )
        )
        val move = Move(Position(5, 5), Position(5, 4))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)
          .either

        //then
        assertM(result)(isLeft(equalTo(KingInCheck(King(White)))))
      },
      testM("Should proceed normally if player is in check but moves his king") {
        // given
        val board = Board(
          positionToPiece = Map(
            Position(4, 4) -> King(White),
            Position(2, 4) -> Queen(Black)
          )
        )
        val move = Move(Position(4, 4), Position(4, 3))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)

        //then
        assertM(result.isSuccess)(isTrue)
      },
      testM("Should proceed normally if all constraints are met") {
        // given
        val board = Board()
        val move = Move(Position(1, 7), Position(2, 5))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)

        //then
        assertM(result.isSuccess)(isTrue)
      },
      testM("Should proceed normally if all constraints are met and should change FirstMovePawn into ActivePawn") {
        // given
        val board = Board()
        val move = Move(Position(2, 6), Position(2, 4))

        //when
        val result = MoveExecutor
          .executeMove(move, board)
          .provideLayer(moveExecutorLayer)

        //then
        assertM(result.map(_.positionToPiece))(hasValues(contains(ActivePawn(White))))
      }
    )

  val moveExecutorLayer = ZEnv.live >>> MoveExecutor.live

}
