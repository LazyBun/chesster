package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.Queen
import zio.test.Assertion._
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test.assert
import zio.test.environment.TestEnvironment

object QueenTest extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Movement Queen")(
      test("It is possible to move up") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move down") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move left") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x - 2, y = initialPosition.y))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move right") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move right up") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move left up") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x - 2, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move right down") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move left down") {
        val initialPosition = Position(4, 4)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = initialPosition.x - 2, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to attack first occupied square") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Queen(White)
        val attackablePiecePosition = Position(6, 6)
        val attackablePiece = ActivePawn(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            attackablePiecePosition -> attackablePiece
          )
        )
        val move = Move(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 2))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isTrue)
      },
      test("It is not possible to attack first occupied square if piece is of player's color") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Queen(White)
        val blockingPiecePosition = Position(6, 6)
        val blockingPiece = ActivePawn(White)
        val board = Board(
          Map(
            initialPosition -> piece,
            blockingPiecePosition -> blockingPiece
          )
        )
        val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 2))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It is not possible to go over a piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Queen(White)
        val blockingPiecePosition = Position(6, 6)
        val blockingPiece = ActivePawn(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            blockingPiecePosition -> blockingPiece
          )
        )
        val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x + 3, y = initialPosition.y + 3))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("Very weird move check") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Queen(White)
        val board = Board(
          Map(
            initialPosition -> piece
          )
        )
        val move = movement.Move(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 1))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It should be impossible to leave board") {
        val initialPosition = Position(7, 7)
        val result = tryQueenMove(initialPosition, initialPosition.copy(x = 8, y = 8))

        //then
        assert(result)(isFalse)
      }
    )

  private def tryQueenMove = TestUtils.tryMovement(Queen(White))(_, _)

}
