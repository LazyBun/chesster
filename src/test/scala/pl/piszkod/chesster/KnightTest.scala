package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.Knight
import zio.test.Assertion._
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test.assert
import zio.test.environment.TestEnvironment

object KnightTest extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Movement Knight")(
      test("It is possible to move 2 up 1 left") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x - 1, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 2 up 1 right") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x + 1, y = initialPosition.y + 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 2 down 1 left") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x - 1, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 2 down 1 right") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x + 1, y = initialPosition.y - 2))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 1 down 2 left") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y - 1))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 1 down 2 right") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x - 2, y = initialPosition.y - 1))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 1 up 2 left") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 1))

        //then
        assert(result)(isTrue)
      },
      test("It is possible to move 1 up 2 right") {
        val initialPosition = Position(4, 4)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = initialPosition.x + 2, y = initialPosition.y + 1))

        //then
        assert(result)(isTrue)
      },
      test("It is not possible to move onto friendly piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Knight(White)
        val friendlyPiecePosition = Position(6, 5)
        val friendlyPiece = ActivePawn(White)
        val board = Board(
          Map(
            initialPosition -> piece,
            friendlyPiecePosition -> friendlyPiece
          )
        )
        val move = Move(initialPosition, friendlyPiecePosition)

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It is possible to move onto enemy piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Knight(White)
        val enemyPiecePosition = Position(6, 5)
        val enemyPiece = ActivePawn(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            enemyPiecePosition -> enemyPiece
          )
        )
        val move = movement.Move(initialPosition, enemyPiecePosition)

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isTrue)
      },
      test("very dumb move") {
        //given
        val initialPosition = Position(4, 4)
        val piece = Knight(White)
        val board = Board(
          Map(
            initialPosition -> piece
          )
        )
        val move = movement.Move(initialPosition, initialPosition.copy(x = 0, y = 0))

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It should be impossible to leave board") {
        val initialPosition = Position(7, 7)
        val result = tryKnightMove(initialPosition, initialPosition.copy(x = 8, y = 9))

        //then
        assert(result)(isFalse)
      }
    )

  private val tryKnightMove = TestUtils.tryMovement(Knight(White))(_, _)

}
