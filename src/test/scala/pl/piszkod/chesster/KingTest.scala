package pl.piszkod.chesster

import pl.piszkod.chesster.Board.Position
import pl.piszkod.chesster.movement.Move
import pl.piszkod.chesster.piece.Color.Black
import pl.piszkod.chesster.piece.Color.White
import pl.piszkod.chesster.piece.ActivePawn
import pl.piszkod.chesster.piece.Bishop
import pl.piszkod.chesster.piece.King
import zio.test.Assertion._
import zio.test.DefaultRunnableSpec
import zio.test.ZSpec
import zio.test.assert
import zio.test.environment.TestEnvironment

object KingTest extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Movement King")(
      test("It is possible to move up") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(4, 5))

        assert(result)(isTrue)
      },
      test("It is possible to move down") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(4, 3))

        assert(result)(isTrue)
      },
      test("It is possible to move left") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(3, 4))

        assert(result)(isTrue)
      },
      test("It is possible to move right") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(5, 4))

        assert(result)(isTrue)
      },
      test("It is possible to move up left") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(3, 5))

        assert(result)(isTrue)
      },
      test("It is possible to move up right") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(5, 5))

        assert(result)(isTrue)
      },
      test("It is possible to move down left") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(3, 3))

        assert(result)(isTrue)
      },
      test("It is possible to move down right") {
        val initialPosition = Position(4, 4)
        val result = tryKingMove(initialPosition, Position(5, 3))

        assert(result)(isTrue)
      },
      test("It is not possible to step onto friendly piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = King(White)
        val friendlyPiecePosition = Position(5, 5)
        val friendlyPiece = ActivePawn(White)
        val board = Board(
          Map(
            initialPosition -> piece,
            friendlyPiecePosition -> friendlyPiece
          )
        )
        val move = Move(initialPosition, friendlyPiecePosition)

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It is possible to step onto enemy piece") {
        //given
        val initialPosition = Position(4, 4)
        val piece = King(White)
        val enemyPiecePosition = Position(5, 5)
        val enemyPiece = ActivePawn(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            enemyPiecePosition -> enemyPiece
          )
        )
        val move = movement.Move(initialPosition, enemyPiecePosition)

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isTrue)
      },
      test("It is not possible to step into checked position") {
        //given
        val initialPosition = Position(4, 4)
        val endPosition = Position(5, 5)
        val piece = King(White)
        val enemyPiecePosition = Position(6, 6)
        val enemyPiece = Bishop(Black)
        val board = Board(
          Map(
            initialPosition -> piece,
            enemyPiecePosition -> enemyPiece
          )
        )
        val move = movement.Move(initialPosition, endPosition)

        //when
        val result = piece.isMoveLegal(move, board)

        //then
        assert(result)(isFalse)
      },
      test("It should be impossible to leave board") {
        val initialPosition = Position(7, 7)
        val result = tryKingMove(initialPosition, initialPosition.copy(x = 8, y = 8))

        //then
        assert(result)(isFalse)
      }
    )

  private def tryKingMove = TestUtils.tryMovement(King(White))(_, _)

}
