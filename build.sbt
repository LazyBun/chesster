ThisBuild / scalaVersion := "2.13.4"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "pl.piszkod"
ThisBuild / organizationName := "piszkod"

lazy val root = (project in file("."))
  .settings(
    name := "chesster",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % "1.0.10",
      "dev.zio" %% "zio-macros" % "1.0.10",
      "dev.zio" %% "zio-test" % "1.0.10" % Test,
      "dev.zio" %% "zio-test-sbt" % "1.0.10" % Test
    ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    scalacOptions ++= Seq(
      "-Ymacro-annotations"
    )
  )
