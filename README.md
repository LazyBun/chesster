# Chesster

Recruitment task for unnamed company. I unfortunately lost the description, but the task was to essentially accept user 
input in format presented in `./moves` folder and output the boards showing the result.

Implementation took about ~8 hours total.

Some of the rules were to be ignored (e.g. _en passant_, promotions)
